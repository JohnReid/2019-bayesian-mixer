\documentclass[usenames, dvipsnames, svgnames, table]{beamer}


%
% Customise beamer
\usetheme{metropolis}  % Simple minimal theme
\usecolortheme[snowy]{owl}  % Black on white
\setbeamertemplate{footline}{}  % Completely remove footline
\setbeamertemplate{frametitle continuation}{[\insertcontinuationcount]}  % nicer numbering
% Uncomment line below to make printable notes
% \setbeameroption{show only notes}
%
% Remove icons from bibliography
% https://tex.stackexchange.com/a/53131/39498
\setbeamertemplate{bibliography item}{}


%
% Compiler specific packages
%
% Check if using XeTeX or not: https://tex.stackexchange.com/a/54597/39498
\usepackage{ifxetex}
\ifxetex{}
  \usepackage{fontspec}
  \usepackage{csquotes}  % Language-specific quotation marks
  \usepackage{polyglossia}
  \setmainlanguage[variant=british]{english}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage[UKenglish, english]{babel}
  \usepackage{lmodern}
\fi


%
% PDF metadata
\usepackage{hyperref}
\hypersetup{%
  pdfauthor={John Reid},
  pdftitle={Active learning with Gaussian processes for dynamical systems},
  pdfkeywords={active learning, dynamical system, Gaussian process},
  pdfcreator={xelatex},
  pdfproducer={xelatex},
}


%
% Math
\usepackage{esdiff}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{commath}
\usepackage{siunitx}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
%\usepackage{amsthm}
%\usepackage{thmtools}
%\usepackage{wasysym}
\usepackage{newtxmath}
%\usepackage{amssymb} % not with newtxmath
%\usepackage{mathabx} % not with newtxmath
\usepackage{bm}


%
% Miscellaneous
%\usepackage{hanging}  % Hanging paragraphs
\usepackage{booktabs}  % Nice tables
\usepackage{layouts}   % Info about layouts
\usepackage{graphicx}  % Graphics
\usepackage[normalem]{ulem} % Underlining
% \usepackage{minted}  % For syntax highlighting source code
\usepackage{setspace}  % For setstretch
\usepackage{calc}  % For widthof()
\usepackage{fontawesome}  % For Twitter symbol, etc...


%
% TikZ
%
\usepackage{tikz}
\usepackage{verbatim}
\usetikzlibrary{arrows,arrows.meta,shapes,decorations.pathmorphing,backgrounds,positioning,fit,graphs,graphs.standard,bayesnet}
%\usetikzlibrary{graph}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}
\usepgflibrary{shapes}
\tikzstyle{int}=[draw, fill=blue!20, minimum size=2em]
\tikzstyle{init} = [pin edge={to-,thin,black}]
% Underlying gray rectangles with rounded corners.
\tikzstyle{background}=[rectangle, fill=gray!10, inner sep=0.2cm, rounded corners=5mm]
\tikzset{%
  myshadow/.style={% custom shadow with tikz
    opacity=.85,
    shadow xshift=0.15,
    shadow yshift=-0.15,
    shade,
    shading=axis,
    shading angle=230},
  >=stealth',
  %Define style for boxes
  punkt/.style={%
          rectangle,
          rounded corners,
          draw=black, very thick,
          text width=7.5em,
          minimum height=2em,
          text centered},
  %Define style for images
  image/.style={%
          text width=7.5em,
          minimum height=2em,
          text centered},
  % Define arrow style
  pil/.style={%
          ->,
          thick,
          shorten <=2pt,
          shorten >=2pt,}
}


%
% biblatex
%
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  backend=biber]{biblatex}
%
% We specify the database.
\addbibresource{2019-Bayesian-Mixer.bib}
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

\newcommand{\myfootcite}[1]{\footnote[frame]{\cite{#1}}}
% \addtobeamertemplate{footnote}{\vspace{-6pt}\advance\hsize-0.5cm}{\vspace{6pt}}
%
\makeatletter
%
% To move footnote citations away from beamer chuff at bottom of page
% Alternative A: footnote rule
% \renewcommand*{\footnoterule}{\kern{} -3pt \hrule \@width{} 2in \kern{} 8.6pt}
%
% Alternative B: no footnote rule
% \renewcommand*{\footnoterule}{\kern 6pt}
%
% From https://tex.stackexchange.com/a/30723/39498
% \def\blfootnote{\gdef\@thefnmark{}\@footnotetext}
\makeatother
%
% A footnote without a marker from
% https://tex.stackexchange.com/questions/30720/footnote-without-a-marker/30726#30726
\newcommand\blfootnote[1]{
  \begingroup
    \renewcommand\thefootnote{}\footnote[frame]{#1}
    \addtocounter{footnote}{-1}
  \endgroup
}



%
% FONTS
%
\usepackage{fix-cm}  % just to avoid some spurious messages
\usefonttheme{professionalfonts}  % using non standard fonts for beamer
%\usefonttheme{serif}  % default family is serif
\setmainfont{TeX Gyre Bonum}


%
% For quotations
% See: https://tex.stackexchange.com/questions/365231/enclose-a-custom-quote-environment-in-quotes-from-csquotes
%
\def\signed#1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
  \hbox{}\nobreak\hfill #1%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

  \newsavebox\mybox{}
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
  {\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}



%
% COMMANDS
%
\newcommand{\bmx}{\bm{x}}
\newcommand{\bmu}{\bm{u}}
\newcommand{\bmw}{\bm{w}}
\newcommand{\bmy}{\bm{y}}
\newcommand{\bmU}{\bm{U}}
\newcommand{\bmmu}{\bm{\mu}}
\newcommand{\bmphi}{\bm{\phi}}
\newcommand{\bmtheta}{\bm{\theta}}
% \newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand{\todo}[1]{\textbf{TODO}: #1}
\newcommand{\hide}[1]{}
%
% Cpp command
%\newcommand{\cpp}{C$^{++}$}
\newcommand{\cpp}{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\bf +}}
%\def\cpp\left\{ C\nolinebreak[4]\hspace{-.05em}\raisebox{.4ex}{\tiny\bf ++}}} \right\}


%
% Document metadata
\title{Bayesian active learning \\ with Gaussian processes}
\author{John Reid \& Lorenz Wernisch}
\date{Wednesday March 13, 2019}
\institute[MRC BSU \& ATI]{MRC Biostatistics Unit \& Alan Turing Institute}


%
% https://tex.stackexchange.com/questions/303018/minipage-reset-indent-how-can-i-set-again-to-standard/303025
% \setlength{\parskip}{\medskipamount}


%
% Document
%
\begin{document}

\maketitle

\begin{frame}<beamer>
  \frametitle{Outline}
  \tableofcontents
\end{frame}




\section{Dynamical systems}


\begin{frame}<beamer>
  \frametitle{Stem cells to megakaryocytes}
  \note{\begin{enumerate}
    \item Transfusions are expensive
    \item Require many platelets
    \item Donor platelets not ideal
    \item Interest in deriving platelets from stem cells
    \item hPSCs are stem cells
    \item Cocktail of genes called transcription factors can produce megakaryocytes
    \item Unfortunately the yield is quite low
    \item Monitor progress using surface markers
    \item Surface markers label each according to its type
    \item Would like to optimise this process, i.e.\ increase yield
  \end{enumerate}}
  NHS spends £280M/year on platelets; $\approx 3 \times 10^{11}$ platelets per transfusion unit
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/megakaryocytes}
    \includegraphics[width=.6\textwidth]{Figures/MK-FoP}
  \end{center}
  \blfootnote{\cite{MoreauLargescaleproductionmegakaryocytes2016, DalbyTranscriptionFactorLevels2018}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Dynamical system}
  \note{\begin{enumerate}
    \item How can we abstract this mathematically?
    \item Treat as unknown dynamical system
    \item A dynamical system is one in which a function describes time dependence
    \item We use discrete dynamical systems, e.g. $f_1, f_2, f_3$
    \item Note inclusion of $u$ in dependencies
    \item See figure, the $u$ is the input and boosts $x_1$
    \item Which in turn affects $x_2$ and $x_3$
    \item Purple line represents target value at time 20 for $x_3$
  \end{enumerate}}
  \begin{columns}
    \begin{column}{.65\textwidth}
      \begin{center}
        \includegraphics[width=\columnwidth]{Figures/toy-dynamics}
      \end{center}
    \end{column}
    \begin{column}{.35\textwidth}
      $u$: control input(s) \\   % chktex 36
      $x$: observed state \\
      \vspace{20pt}
      Target: $x_{3, 20} = 780$
    \end{column}
  \end{columns}
  \vspace{10pt}
  \begin{align*}
    \begin{bmatrix} x_{1,t+1} \\ x_{2,t+1} \\ x_{3,t+1} \end{bmatrix}
    =
    \begin{bmatrix} x_{1,t} \\ x_{2,t} \\ x_{3,t} \end{bmatrix}
    +
    \begin{bmatrix}
      f_1(u_{t}, x_{1,t}, x_{2,t}, x_{3,t}) \\
      f_2(u_{t}, x_{1,t}, x_{2,t}, x_{3,t}) \\
      f_3(u_{t}, x_{1,t}, x_{2,t}, x_{3,t})
    \end{bmatrix}
  \end{align*}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Rules of the game}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  Want $\argmin_u \mathcal{L}(u, x)$ where for example
  \begin{align*}
    \mathcal{L}(u, x) = \norm{x_{3,20} - 780}^2 + \lambda \sum_t |u_t|
  \end{align*}
  but experiments are expensive

  Possible strategies:
  \begin{itemize}
    \item Random search
    \item Grid search
    \item Manual guessing
    \item Bayesian optimisation
    \item Active learning
  \end{itemize}
\end{frame}



\section{Gaussian processes}

\begin{frame}<beamer>
  \frametitle{Gaussian processes}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{block}{}
    GPs are the infinite-dimensional analogues of multivariate Gaussians
  \end{block}
  \vspace*{-8pt}
  \begin{align*}
    \bmy \sim \mathcal{N}(\bmmu, \Sigma)
  \end{align*}
  \begin{center}
    \includegraphics[width=.6\textwidth]{Figures/Generated/mvn}
  \end{center}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Priors over functions}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  % textwidth in inches \printinunitsof{in}\prntlen{\textwidth}
  \begin{columns}[t]
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=\columnwidth]{Figures/Generated/prior2d}
        \begin{align*}
          f(.) &\sim \mathcal{GP}(m(.), k(.,.)) \\
          y(x) &\sim \mathcal{N}(f(x), \sigma_n^2) \\
          m&: \mathcal{X} \to \mathbb{R} \\
          k&: \mathcal{X} \times \mathcal{X} \to \mathbb{R}
        \end{align*}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=\columnwidth]{Figures/Generated/prior10d}
        \includegraphics[width=\columnwidth]{Figures/Generated/prior}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Properties}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{block}{Defined by second-order statistics}
    Uniquely specified by mean and covariance functions
  \end{block}
  \begin{block}{Marginalisation}
    Any finite-dimensional subset of a GP is multivariate Gaussian
  \end{block}
  \begin{block}{X can be any domain}
    $k: \mathcal{X} \times \mathcal{X} \to \mathbb{R}$ only needs to be a valid covariance function (kernel)
  \end{block}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Gaussian process posteriors}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \vspace*{-20pt}
  \begin{align*}
    f(.) &\sim \mathcal{GP}(m(.), k(.,.)) \\
    y(x) &\sim \mathcal{N}(f(x), \sigma_n^2)
  \end{align*}
  \begin{center}
    \includegraphics[width=.48\textwidth]{Figures/Generated/posterior-samples}
    \includegraphics[width=.48\textwidth]{Figures/Generated/posterior}
  \end{center}
  when $m(.) = 0$
  \begin{align*}
    f(x^*) &\sim \mathcal{N}(K_{x^*x}K_{xx}^{-1}y, \Sigma), \qquad \Sigma = K_{x^*x^*} - K_{x^*x}K_{xx}^{-1}K_{xx^*}
  \end{align*}
\end{frame}




\begin{frame}<beamer>
  \frametitle{Covariance functions}
  \note{\begin{enumerate}
    \item The GP mean function is normally not interesting
    \item Typically all interesting modelling decisions are made in the covariance function
    \item Both in its choice and its parameters
    \item Functions can be constrained to be periodic
  \end{enumerate}}
  \begin{center}
    \includegraphics[height=.8\textheight]{Figures/Generated/kernel-comparison}
  \end{center}
  \vspace{-20pt}
  \blfootnote{Kernel cookbook:~\url{http://www.cs.toronto.edu/~duvenaud/cookbook/}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Hyper-parameters}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{align*}
    k(x, x') &= \sigma_f^2 \exp \bigg[-\frac{{(x-x')}^2}{2l^2}\bigg]
  \end{align*}
  \begin{center}
    \includegraphics[height=.6\textheight]{Figures/Generated/kernel-lengthscales}
  \end{center}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Automatic relevance determination}
  \note{\begin{enumerate}
  \end{enumerate}}
  30 samples from
  \begin{align*}
    f(x_1, x_2, x_3) = 5 \sin(0.7 x_1) + 0.5 x_2 + \epsilon
  \end{align*}
  where $\epsilon \sim \mathcal{N}(0, 1)$
  \vspace{10pt}
  \begin{columns}[c]
    %
    \column{.5\textwidth}
      \includegraphics[width=\columnwidth]{Figures/ard}
    %
    \column{.5\textwidth}
      $l_d$: lengthscale for $d$'th dimension \\
      \vspace{20pt}
      \begin{tabular}{rlll}
        \toprule
           $1/l_d$ & $x_1$ & $x_2$ & $x_3$ \\
        \midrule
        non-linear & 0.21  & 0     & 0   \\
            linear & 0     & 0.35  & 0   \\
        \bottomrule
      \end{tabular} \\
      \vspace{20pt}
      Estimated $\epsilon = 0.92$
  \end{columns}
\end{frame}


\begin{frame}<beamer>
  \frametitle{GP resources}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}

  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{center}
        % \cite{rasmussen_gaussian_2006} \\
        \includegraphics[width=\textwidth]{Figures/rwcover}
      \end{center}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{itemize}
        \item Gaussian processes
        \begin{itemize}
          \item GPflow: TensorFlow
          \item GPy: python
          \item GPyOpt: python
          \item GPml: Matlab
        \end{itemize}
        \item Probabilistic programming languages
        \begin{itemize}
          \item Stan: widely used; HMC
          \item Edward: flexible; TensorFlow integration
        \end{itemize}
        \item Bayesian optimisation
        \begin{itemize}
          \item Spearmint
          \item HPOLib
        \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
  \blfootnote{\url{http://www.gaussianprocess.org/}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{GP recap}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{itemize}
    \item Principled Bayesian models
    \item Full predictive uncertainty
    \item Extremely flexible
    \item `Default' covariance functions typically work well
    \item Care needed for extrapolation
    \item Sparse approximations for big data
    \item Automatic relevance determination
    \item Just multivariate Gaussians
  \end{itemize}
\end{frame}



\section{Related approaches}

\begin{frame}<beamer>
  \frametitle{Bayesian optimisation}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[height=.72\textheight]{Figures/practical-Bayesian-opt}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item Minimisation problem
        \item Expensive evaluations
        \item Improvement on grid-search
        \item Exploitation vs.\ exploration
        \item Parallel versions
      \end{itemize}
      % \cite{srinivas_gaussian_2009, snoek_practical_2013, snoek_scalable_2015}
      % \cite{swersky_multi-task_2013, swersky_freeze-thaw_2014, gardner_discovering_2017}
    \end{column}
  \end{columns}
  \blfootnote{\cite{SnoekPracticalBayesianOptimization2013}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{PILCO\@: reinforcement learning}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{center}
    \includegraphics[height=.7\textheight]{Figures/pilco-demo}
  \end{center}
  \blfootnote{\url{https://www.youtube.com/watch?v=XiigTGKZfks}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Propagating uncertainty}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{center}
    \includegraphics[width=.97\textwidth]{Figures/PILCO}
  \end{center}
  \begin{align*}
    \bmx \mapsto \pi(\bmx) = \bmu, \qquad J^\pi(\theta) = \sum_{t=0}^T \mathbb{E}_{\bmx_t}[c(\bmx_t)]
  \end{align*}
  \blfootnote{\cite{ICML2011Deisenroth_323}}
\end{frame}



\section{Active learning}

\begin{frame}<beamer>
  \frametitle{Active learning}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{aquote}{Wikipedia}
    Active learning is a special case of machine learning in which a learning
    algorithm is able to interactively query the user (or some other
    information source) to obtain the desired outputs at new data points. In
    statistics literature it is sometimes also called optimal experimental
    design.
  \end{aquote}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Active learning}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \input{Figures/active-learning.tex}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Model: Gaussian process dynamical system (GPDS)}
  \note{\begin{enumerate}
    \item $d$ is a parameterised distance function with a lengthscale for each dimension
  \end{enumerate}}
  \begin{align*}
    x_{i,t+1} &= x_{i,t} + f_i(\bmu_t, \bmx_t) \\
    f_i &\sim \mathcal{GP}(m(.), k(.,.)) \\
    m(\bmu_t, \bmx_t) &= 0 \\
    k(\bmu_t, \bmx_t, \bmu_t', \bmx_t') &=
      \theta \exp[- \frac{1}{2} {d((\bmu_t, \bmx_t), (\bmu_t', \bmx_t'))}^2]
  \end{align*}
  $d$ is a parameterised distance function between $(\bmu_t, \bmx_t)$ and $(\bmu_t', \bmx_t')$
\end{frame}


\begin{frame}<beamer>
  \frametitle{Simulate: random trajectories}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{itemize}
    \item \emph{Bad idea}: use GP posterior means
    \item \emph{Better idea}: sample random trajectories from GP posterior
  \end{itemize}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/random-traj}
  \end{center}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Dataflow graphs}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{columns}[t]
    %
    \column{.5\textwidth}
    \\
    \input{Figures/dataflow.tex}
    %
    \column{.5\textwidth}
    Unroll in time
    \begin{align*}
      x^* &= \begin{bmatrix} u_t \\ x_{1,t} \\ x_{2,t} \\ x_{3,t} \end{bmatrix} \\
      x_{1,t+1} &\sim \mathcal{N}(x_{1,t} + K_{x^*x}K_{xx}^{-1}y, \Sigma) \\
      \Sigma &= K_{x^*x^*} - K_{x^*x}K_{xx}^{-1}K_{xx^*}
    \end{align*}

    TensorFlow implementation includes Cholesky decomposition
  \end{columns}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Optimise: use GP approximation}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  Expected loss
  \begin{align*}
    \mathbb{E}_{x \sim \textnormal{GPDS}_u}[\mathcal{L}(u, x)] &\approx \frac{1}{M} \sum_{m=1}^M \mathcal{L}(u, x_m)
  \end{align*}
  Gradients available from TensorFlow implementation
  \begin{align*}
    \argmin_u \mathbb{E}_{x \sim \textnormal{GPDS}_u}[\mathcal{L}(u, x)]
  \end{align*}
  Broyden–Fletcher–Goldfarb–Shanno (BFGS) algorithm
\end{frame}



\section{Results}


\begin{frame}<beamer>
  \frametitle{%
    \raisebox{-1.8ex}{\includegraphics[height=1cm]{Figures/bio-models}}
    Dynamical system: BioModels database}
  \note{\begin{enumerate}
    \item Bistable switch has two stable states: stem cell and differentiated
    \item Stem cell state has OCT4, SOX2 and NANOG expression
    \item Stable due to positive feedback loops
    \item Control input A+ can force system into stem cell state
    \item We don't use input B
    \item Target NANOG = 50 at $t=20$
  \end{enumerate}}
  \begin{columns}[b]
    %
    \column{.5\textwidth}
    \centering
    bistable switch \\
    \vspace{12pt}
    \includegraphics[width=\columnwidth]{Figures/Chickarmane-network}
    %
    \column{.5\textwidth}
    \centering
    ODE model \\
    \vspace{12pt}
    \includegraphics[width=\columnwidth]{Figures/Chickarmane-eqns} \\
    \vspace{12pt}
    \texttt{tellurium} python simulator
    %
  \end{columns}
  \blfootnote{\cite{ChelliahBioModelstenyearanniversary2015, ChickarmaneTranscriptionalDynamicsEmbryonic2006}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Stem cell bistable switch}
  \note{\begin{enumerate}
  \end{enumerate}}
  \centering
  \includegraphics[height=.9\textheight]{Figures/nanog-50}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Incoherent feed-forward loop}
  \note{\begin{enumerate}
    \item A has additive effect on U0
    \item Incoherent as U0 has direct '+'ve effect on X2 and indirect '-'ve effect via X1
    \item Target $X_2 = 780$ at $t = 20$
  \end{enumerate}}
  \begin{columns}[b]
    %
    \column{.3\textwidth}
    \input{Figures/ffl.tex}
    %
    \column{.7\textwidth}
    \includegraphics[width=\columnwidth]{Figures/ffl-780}
    %
  \end{columns}
\end{frame}


\begin{frame}<beamer>
  \frametitle{GP for $f_2(u_0, x_1)$}
  \note{\begin{enumerate}
  \end{enumerate}}
  \centering
  \begin{columns}[t]
    %
    \column{.5\textwidth}
      After initialisation \\
      \includegraphics[width=1.2\columnwidth]{Figures/gp-f3-initial-uncertainty}
    %
    \column{.5\textwidth}
      After four maximisation experiments \\
      \includegraphics[width=1.2\columnwidth]{Figures/gp-f3-after4-uncertainty}
  \end{columns}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Latent dimensionality}
  \note{\begin{enumerate}
  \end{enumerate}}
  \centering
  \includegraphics[width=\textwidth]{Figures/latent-dims}
  \begin{align*}
    f(y) &= \diff{y}{t}
  \end{align*}
  not representable in 1D as identical $y$ are mapped to different $f(y)$
\end{frame}


\begin{frame}<beamer>
  \frametitle{Takens' theorem}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{columns}[b]
    %
    \column{.7\textwidth}
      \centering
      \includegraphics[width=\columnwidth]{Figures/Takens-thm}
    %
    \column{.3\textwidth}
      Cannot observe $x(t), y(t), z(t)$ \\
      \vspace{20pt}
      However
      \begin{align*}
        \begin{bmatrix}
          y(t) \\ y(t-d) \\ y(t-2d) \\ \dots \\ y(t-Kd)
        \end{bmatrix}
      \end{align*}
      sufficient
    %
  \end{columns}
  \blfootnote{\cite{citeulike:2735031}, \url{https://math.stackexchange.com/q/2262961/203036}}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Wrap-up}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{itemize}
    \item Dynamic control widely applicable
    \item Accounting for uncertainty is essential
    \item Regularisation by expectation
    \item Converges quickly
    \item TensorFlow permits flexible implementation
    \item Compare to Bayesian optimisation
    \item Automatic relevance determination
  \end{itemize}
\end{frame}


\begin{frame}<beamer>
  \frametitle{Acknowledgements}
  \note{\begin{enumerate}
    \item
  \end{enumerate}}
  \begin{columns}[t]
    %
    \begin{column}{0.25\textwidth}
        \includegraphics[height=1in]{Figures/Lorenz_Wernisch_bw} \\
        Lorenz Wernisch \\
    \end{column}
    %
    \begin{column}{0.25\textwidth}
        \includegraphics[height=1in]{Figures/John} \\
        John Reid \\
        \faTwitter{} \ \texttt{@\_\_Reidy\_\_}
    \end{column}
    %
    \begin{column}{0.25\textwidth}
        \includegraphics[height=1in]{Figures/cedric-ghevaert} \\
        Cédric Ghevaert
    \end{column}
    %
    \begin{column}{0.25\textwidth}
        \includegraphics[height=1in]{Figures/thomas-moreau} \\
        Thomas Moreau
    \end{column}
  \end{columns}
  Code: \\
  \url{https://github.com/JohnReid/dynlearn} \\
  \vspace{10pt}
  Slides: \\
  \url{https://bitbucket.org/JohnReid/2019-bayesian-mixer/}
\end{frame}


\begin{frame}<beamer>[allowframebreaks]
  \frametitle{References}
  \renewcommand*{\bibfont}{\footnotesize}
  \printbibliography[heading=none]{}
\end{frame}

\end{document}
